﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;


namespace M09UF2E1
{
   public class Prova
    {
        private bool ontherun = false;


        public void ProvaProva(int lines)
        {
            ThreadNumber(lines);
        }

        public void NumeroDeLinies(object lines)
        {
            string liniesString = Convert.ToString(lines);
            
            int linesnum=0;

            int lineposition = 0;

            do
            {
                if (linesnum == liniesString[lineposition] && lineposition < liniesString.Length)
                {
                    lineposition++;
                   
                    linesnum++;
                }
                else
                {
                    ontherun = true;
                }

            } while (ontherun != true);
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine($"El text té {lines} línies.");
            
        }

        public void ThreadNumber(int linies)
        {

            Thread ThreadWithParam = new Thread(NumeroDeLinies);
            ThreadWithParam.Start(linies);

        }

    }

}
