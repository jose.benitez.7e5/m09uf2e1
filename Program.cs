﻿using System;
using System.Threading;

namespace M09UF2E1
{
    class Program
    {
        private static int lines;

        static void Main()
        {
            Onethread();
        }

        static void Onethread()
        {           
            Thread myFirstThread = new Thread(ThreadwithParam);
            myFirstThread.Start("cuptext1.txt");
            Thread.Sleep(20);
          
            Thread mySecondThread = new Thread(ThreadwithParam);
            mySecondThread.Start("cuptext2.txt");
            
            Thread myThirdThread = new Thread(ThreadwithParam);
            myThirdThread.Start("cuptext3.txt");
          
            Thread myFourThread = new Thread(ThreadwithParam);
            myFourThread.Start("cuptext4.txt");
           
        }


        static void ThreadwithParam(object cupfile)
        {
            string cuptext = @"..\..\..\cuptext\" + cupfile;
            lines = NumberofLines(cuptext);
           
            var prova = new Prova();

            prova.ProvaProva(lines);
        }


        private static int NumberofLines(string cupfile)
        {

            string[] lines = System.IO.File.ReadAllLines(cupfile);

            return lines.Length;
        }


    }

}

